# umatibit
Umatibit - Buy bitcoin

=======
Umatibit website

## How to setup staging deployment
The staging environment is hosted on Openshift. It's powered by git and hence deployments are based on git push trigger.
The pre-requisites for being able to use this environment are:

1. Openshift account
2. Public key added to account
3. git

Clone the repo from source `git clone ssh://58496ceb7628e1a745000203@umatibit-bonga.rhcloud.com/~/git/umatibit.git/`

Enter repo

`cd umatibit`

Add new upstream **develop** branch
```
# create new branch
git checkout -b develop

# add remote repository
git remote add develop ssh://58496ceb7628e1a745000203@umatibit-bonga.rhcloud.com/~/git/umatibit.git

# fetch branch contents
git pull develop

# now setup upstream
git branch --set-upstream-to=develop/master

# confirm origin
git remote -v
```
