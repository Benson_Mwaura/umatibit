<?php
class Pages extends CI_Controller {
	public function view($page = 'home')
	{
		$this->load->helper('url');
		if(!file_exists(APPPATH.'views/pages/'.$page.'.php'))
		{
			// Static page not found
			show_404();
		}

		$data['title'] = ucfirst($page);	//Capitalize first letter

		$this->load->view('templates/header', $data);
		$this->load->view('pages/'.$page, $data);
		//$this->load->view('templates/footer', $data);
	}
}
?>